import { getCitiesByName, getWeatherByCityId } from '@/api'
import City from '@/models/city'
import { CityRequest } from '@/models/city/CityRequest'
import { CityResponse } from '@/models/city/CityResponse'
import { WeatherRequest } from '@/models/weather/WeatherRequest'
import { WeatherResponse } from '@/models/weather/WeatherResponse'

export default class WeatherService {
  async getCitiesByName(params: CityRequest): Promise<CityResponse> {
    return this.formatCityResponse(await getCitiesByName(params))
  }

  async getWeatherByCityId(params: WeatherRequest): Promise<WeatherResponse> {
    return this.formatWeatherResponse(await getWeatherByCityId(params))
  }

  saveCities(cities: Array<City>): void {
    localStorage.setItem('cities', JSON.stringify(cities))
  }

  getSavedCity(): Array<City> {
    const storageResponse = localStorage.getItem('cities')
    return storageResponse ? JSON.parse(storageResponse) : []
  }

  private formatCityResponse(response: CityResponse) {
    response.list.map((city: City) => this.formatCityItem(city))
    return response
  }

  private formatWeatherResponse(response: WeatherResponse) {
    return this.formatCityItem(response)
  }

  private formatCityItem(city: WeatherResponse) {
    const country = city.sys?.country ? `, ${city.sys.country}` : ''
    const temp = city.main?.temp ? `,  ${city.main.temp}°C` : ''
    city.title = `${city.name + country + temp}`
    return city
  }
}
