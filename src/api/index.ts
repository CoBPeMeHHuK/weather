import { CityRequest } from '@/models/city/CityRequest'
import { CityResponse } from '@/models/city/CityResponse'
import { WeatherRequest } from '@/models/weather/WeatherRequest'
import { WeatherResponse } from '@/models/weather/WeatherResponse'
import { API_KEY } from '../config'

export const getCitiesByName = function (
  params: CityRequest
): Promise<CityResponse> {
  return fetch(
    `http://api.openweathermap.org/data/2.5/find?q=${params.cityName}&appid=${API_KEY}&lang=${params.lang}&units=metric`
  ).then((data) => data.json())
}

export const getWeatherByCityId = function (
  params: WeatherRequest
): Promise<WeatherResponse> {
  return fetch(
    `http://api.openweathermap.org/data/2.5/weather?id=${params.id}&appid=${API_KEY}&lang=${params.lang}&units=metric`
  ).then((data) => data.json())
}
