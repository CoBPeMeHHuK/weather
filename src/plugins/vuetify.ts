import Vue from 'vue'
import Vuetify from 'vuetify/lib/framework'
import ru from '../locale/ru'
import en from '../locale/en'

Vue.use(Vuetify)

export default new Vuetify({
  lang: {
    locales: { ru, en },
    current: 'ru',
  },
})
