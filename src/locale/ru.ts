import { ru } from 'vuetify/src/locale'

export default {
  ...ru,

  notFoundMessage: 'Город не найден',
  humidity: 'Влажность',
  feelsLike: 'Чувствуется как',
  chooseCity: 'Выберите город:',
  previousSelectedCity: 'Ранее выбранные города',
  chooseLang: 'Выберите язык',
  tempMin: 'Температура мин.',
  tempMax: 'Температура макс.',
  pressure: 'Давление',
  lengthValueError: 'Поле не может быть пустым',
}
