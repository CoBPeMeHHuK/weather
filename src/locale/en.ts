import { en } from 'vuetify/src/locale'

export default {
  ...en,

  notFoundMessage: 'Not found city',
  humidity: 'Humidity',
  feelsLike: 'Feels like',
  chooseCity: 'Choose city:',
  previousSelectedCity: 'Previous selected city',
  chooseLang: 'Choose lang',
  tempMin: 'Temperature min.',
  tempMax: 'Temperature max.',
  pressure: 'Pressure',
  lengthValueError: 'Field cannot be empty',
}
