import City from '../city'

export interface WeatherResponse extends City {
  cod?: string | number
  message?: string
}
