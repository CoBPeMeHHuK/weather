export type WeatherRequest = {
  id: number | null
  lang: string
}
