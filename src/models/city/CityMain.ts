export class CityMain {
  constructor() {
    this.feels_like = null
    this.humidity = null
    this.pressure = null
    this.temp = null
    this.temp_max = null
    this.temp_min = null
  }
  feels_like: number | null
  humidity: number | null
  pressure: number | null
  temp: number | null
  temp_max: number | null
  temp_min: number | null
}
