export class CityWeather {
  constructor() {
    this.id = null
    this.description = null
  }
  id: number | null
  description: string | null
}
