import City from './'

export type CityResponse = {
  list: Array<City>
  count: number
  cod: string
  message: string
}
