import { CityMain } from './CityMain'
import { CitySys } from './CitySys'
import { CityWeather } from './CityWeather'

export default class City {
  constructor() {
    this.id = null
    this.name = ''
    this.title = ''
    this.rain = ''
    this.snow = ''
    this.main = new CityMain()
    this.weather = [new CityWeather()]
    this.sys = new CitySys()
  }
  id: number | null
  name: string
  title: string
  rain: string
  snow: string
  main: CityMain
  weather: Array<CityWeather>
  sys: CitySys
}
